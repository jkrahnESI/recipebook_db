USE [master]
GO
/****** Object:  Database [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf]    Script Date: 2/2/2021 5:40:19 PM ******/
CREATE DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'recipe_db', FILENAME = N'C:\Users\jkrahn\source\repos\RecipeBook_Db\recipe_db.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'recipe_db.log', FILENAME = N'C:\Users\jkrahn\source\repos\RecipeBook_Db\recipe_db_log.ldf' , SIZE = 768KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET ARITHABORT OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET AUTO_SHRINK ON 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET  DISABLE_BROKER 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET  MULTI_USER 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET DB_CHAINING OFF 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET QUERY_STORE = OFF
GO
USE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf]
GO
/****** Object:  UserDefinedTableType [dbo].[IngredientList]    Script Date: 2/2/2021 5:40:19 PM ******/
CREATE TYPE [dbo].[IngredientList] AS TABLE(
	[ingredient_name] [nvarchar](20) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[RecipeIdList]    Script Date: 2/2/2021 5:40:19 PM ******/
CREATE TYPE [dbo].[RecipeIdList] AS TABLE(
	[recipe_id] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[RecipeNumIngredients]    Script Date: 2/2/2021 5:40:19 PM ******/
CREATE TYPE [dbo].[RecipeNumIngredients] AS TABLE(
	[recipe_id] [int] NOT NULL,
	[num_ingredients] [int] NULL,
	PRIMARY KEY CLUSTERED 
(
	[recipe_id] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Ingredients]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ingredients](
	[ingredient_id] [int] IDENTITY(1,1) NOT NULL,
	[ingredient_type_id] [int] NOT NULL,
	[ingredient_name] [nvarchar](50) NULL,
	[staple] [bit] NULL,
 CONSTRAINT [PK__Ingredie__B0E453CF11F70BB1] PRIMARY KEY CLUSTERED 
(
	[ingredient_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ingredient_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ingredient_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recipe_Ingredients]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recipe_Ingredients](
	[recipe_id] [int] NOT NULL,
	[ingredient_id] [int] NOT NULL,
	[ingredient_amount] [nvarchar](50) NULL,
	[ingredient_prep] [nvarchar](50) NULL,
	[ingredient_optional] [bit] NOT NULL,
 CONSTRAINT [PK_Recipe_Ingredients] PRIMARY KEY CLUSTERED 
(
	[recipe_id] ASC,
	[ingredient_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Recipe_Ingredient] UNIQUE NONCLUSTERED 
(
	[recipe_id] ASC,
	[ingredient_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[func_Get_Recipe_Ids_From_Ingredient]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[func_Get_Recipe_Ids_From_Ingredient]
(	
	-- Add the parameters for the function here
	@ingredient_name nvarchar(20),
	@recipe_id_list RecipeIdList READONLY
)
RETURNS TABLE 
AS
RETURN 
	SELECT [@recipe_id_list].recipe_id
	FROM @recipe_id_list
	INNER JOIN Recipe_Ingredients ON [@recipe_id_list].recipe_id=Recipe_Ingredients.recipe_id
	INNER JOIN Ingredients ON Recipe_Ingredients.ingredient_id = Ingredients.ingredient_id
	WHERE ingredient_name = @ingredient_name
GO
/****** Object:  Table [dbo].[Cuisines]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cuisines](
	[cuisine_id] [int] IDENTITY(1,1) NOT NULL,
	[cuisine_name] [nvarchar](50) NULL,
 CONSTRAINT [PK__Cuisines__3197C6F4E87BB68E] PRIMARY KEY CLUSTERED 
(
	[cuisine_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[cuisine_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ingredient_Types]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ingredient_Types](
	[ingredient_type_id] [int] IDENTITY(1,1) NOT NULL,
	[ingredient_type] [nvarchar](50) NULL,
 CONSTRAINT [PK__Ingredie__DF9AAD71A4EE66F6] PRIMARY KEY CLUSTERED 
(
	[ingredient_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ingredient_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recipes]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recipes](
	[recipe_id] [int] IDENTITY(1,1) NOT NULL,
	[recipe_name] [nvarchar](50) NULL,
	[recipe_description] [nvarchar](50) NULL,
	[recipe_instructions] [nvarchar](max) NULL,
	[recipe_prep_time] [time](0) NULL,
	[recipe_cook_time] [time](0) NULL,
	[recipe_cuisine_id] [int] NULL,
 CONSTRAINT [PK__Recipes__3571ED9B9326BFB3] PRIMARY KEY CLUSTERED 
(
	[recipe_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [Recipes_recipe_name_unique]    Script Date: 2/2/2021 5:40:19 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Recipes_recipe_name_unique] ON [dbo].[Recipes]
(
	[recipe_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Ingredients] ADD  CONSTRAINT [DF_Ingredients_ingredient_type_id]  DEFAULT ((1)) FOR [ingredient_type_id]
GO
ALTER TABLE [dbo].[Recipe_Ingredients] ADD  CONSTRAINT [DF_Recipe_Ingredients_ingredient_optional]  DEFAULT ((0)) FOR [ingredient_optional]
GO
ALTER TABLE [dbo].[Recipes] ADD  CONSTRAINT [DF__Recipes__recipe___2E1BDC42]  DEFAULT ((1)) FOR [recipe_cuisine_id]
GO
ALTER TABLE [dbo].[Ingredients]  WITH CHECK ADD  CONSTRAINT [FK_Ingredients_ToIngredient_Types] FOREIGN KEY([ingredient_type_id])
REFERENCES [dbo].[Ingredient_Types] ([ingredient_type_id])
GO
ALTER TABLE [dbo].[Ingredients] CHECK CONSTRAINT [FK_Ingredients_ToIngredient_Types]
GO
ALTER TABLE [dbo].[Recipe_Ingredients]  WITH CHECK ADD  CONSTRAINT [FK_Recipe_Ingredients_ToIngredients] FOREIGN KEY([ingredient_id])
REFERENCES [dbo].[Ingredients] ([ingredient_id])
GO
ALTER TABLE [dbo].[Recipe_Ingredients] CHECK CONSTRAINT [FK_Recipe_Ingredients_ToIngredients]
GO
ALTER TABLE [dbo].[Recipe_Ingredients]  WITH CHECK ADD  CONSTRAINT [FK_Recipe_Ingredients_ToRecipes] FOREIGN KEY([recipe_id])
REFERENCES [dbo].[Recipes] ([recipe_id])
GO
ALTER TABLE [dbo].[Recipe_Ingredients] CHECK CONSTRAINT [FK_Recipe_Ingredients_ToRecipes]
GO
ALTER TABLE [dbo].[Recipes]  WITH CHECK ADD  CONSTRAINT [FK_Recipes_To_Cuisines] FOREIGN KEY([recipe_cuisine_id])
REFERENCES [dbo].[Cuisines] ([cuisine_id])
GO
ALTER TABLE [dbo].[Recipes] CHECK CONSTRAINT [FK_Recipes_To_Cuisines]
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_All_Recipes]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_All_Recipes] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Recipes
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Recipe_Id]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Recipe_Id] 
	-- Add the parameters for the stored procedure here
	@recipe_name nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 recipe_id 
	FROM Recipes 
	WHERE recipe_name = @recipe_name
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Recipe_Ingredients_From_Recipe_Id]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Recipe_Ingredients_From_Recipe_Id]
	-- Add the parameters for the stored procedure here
	@recipe_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Ingredients.ingredient_name,	Recipe_Ingredients.ingredient_amount, 
	Recipe_Ingredients.ingredient_prep, Recipe_Ingredients.ingredient_optional
	FROM Recipe_Ingredients
	JOIN Ingredients ON Ingredients.ingredient_id=Recipe_Ingredients.ingredient_id
	WHERE recipe_id = @recipe_id
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Recipes_From_Ingredient]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Recipes_From_Ingredient] 
	-- Add the parameters for the stored procedure here
	@ingredient_name nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT recipe_name
	FROM Recipes
	INNER JOIN Recipe_Ingredients ON Recipes.recipe_id=Recipe_Ingredients.recipe_id
	INNER JOIN Ingredients ON Recipe_Ingredients.ingredient_id=Ingredients.ingredient_id
	WHERE @ingredient_name=Ingredients.ingredient_name
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Recipes_On_Hand]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Recipes_On_Hand]
	-- Add the parameters for the stored procedure here
	@ingredient_names dbo.IngredientList READONLY
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ingredient_name nvarchar(20)
	DECLARE @rec_ingr RecipeNumIngredients
	DECLARE @oh_ingr RecipeNumIngredients
	DECLARE @list RecipeIdList
	INSERT INTO @list SELECT recipe_id FROM Recipes
	
	INSERT INTO @rec_ingr 
		SELECT recipe_id, COUNT(recipe_id)
		FROM Recipe_Ingredients
		GROUP BY recipe_id

	INSERT INTO @oh_ingr
		SELECT recipe_id, COUNT(recipe_id)
		FROM Recipe_Ingredients
		INNER JOIN Ingredients ON Recipe_Ingredients.ingredient_id=Ingredients.ingredient_id
		INNER JOIN @ingredient_names ON Ingredients.ingredient_name=[@ingredient_names].ingredient_name
		GROUP BY recipe_id

	SELECT recipe_name
	FROM Recipes
	INNER JOIN @oh_ingr ON [@oh_ingr].recipe_id=Recipes.recipe_id
	INNER JOIN @rec_ingr ON [@oh_ingr].recipe_id=[@rec_ingr].recipe_id
	WHERE [@oh_ingr].num_ingredients >= [@rec_ingr].num_ingredients * 1
		
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Recipes_With_Specific_IngredientList]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Get_Recipes_With_Specific_IngredientList] 
	@ingredient_names dbo.IngredientList READONLY
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ingredient_name nvarchar(50)
	DECLARE @list RecipeIdList
	INSERT INTO @list SELECT recipe_id FROM Recipes
	
	DECLARE db_cursor CURSOR FOR
	SELECT ingredient_name
	FROM @ingredient_names

	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @ingredient_name
	
	WHILE @@FETCH_STATUS=0
	BEGIN
		DELETE FROM @list
		WHERE [@list].recipe_id NOT IN (
			SELECT recipe_id
			FROM [dbo].func_Get_Recipe_Ids_From_Ingredient(@ingredient_name, @list)
			)

		FETCH NEXT FROM db_cursor INTO @ingredient_name
	END

	SELECT recipe_name
	FROM Recipes
	INNER JOIN @list ON Recipes.recipe_id=[@list].recipe_id
	
END

CLOSE db_cursor
DEALLOCATE db_cursor
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Recipes_With_Two_Ingredients]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Get_Recipes_With_Two_Ingredients] 
	@ingredient_name_1 nvarchar(20),
	@ingredient_name_2 nvarchar(20)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @list RecipeIdList
	INSERT INTO @list SELECT recipe_id FROM Recipes

	DELETE FROM @list
	WHERE [@list].recipe_id NOT IN (
		SELECT recipe_id
		FROM [dbo].func_Get_Recipe_Ids_From_Ingredient(@ingredient_name_1, @list)
		)

	DELETE FROM @list
	WHERE [@list].recipe_id NOT IN (
		SELECT recipe_id
		FROM [dbo].func_Get_Recipe_Ids_From_Ingredient(@ingredient_name_2, @list)
		)

	SELECT recipe_name
	FROM Recipes
	INNER JOIN @list ON Recipes.recipe_id=[@list].recipe_id
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Whole_Recipe_From_Name]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Whole_Recipe_From_Name] 
	-- Add the parameters for the stored procedure here
	@recipe_name nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Recipes.recipe_name, Recipes.recipe_description, Recipes.recipe_instructions,
	Recipes.recipe_prep_time, Recipes.recipe_cook_time, Ingredients.ingredient_name,
	Recipe_Ingredients.ingredient_amount, Recipe_Ingredients.ingredient_prep, Recipe_Ingredients.ingredient_optional
	FROM Recipe_Ingredients
	JOIN Recipes ON Recipes.recipe_id=Recipe_Ingredients.recipe_id
	JOIN Ingredients ON Ingredients.ingredient_id=Recipe_Ingredients.ingredient_id
	WHERE recipe_name = @recipe_name
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Reseed_Cuisines]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Reseed_Cuisines]
AS
BEGIN
    DELETE FROM Cuisines
    DBCC CHECKIDENT ('Cuisines', RESEED, 0)  
    -- body of the stored procedure
    INSERT INTO Cuisines
    VALUES
        (NULL),
        (N'American'),
        (N'Italian'),
        (N'French'),
        (N'German'),
        (N'Mexican'),
        (N'Jamaican'),
        (N'Brazilian'),
        (N'Thai'),
        (N'Chinese'),
        (N'Asian Fusion'),
        (N'Japanese'),
        (N'Korean'),
        (N'Thai'),
        (N'Vietnamese'),
        (N'Lao')
END
-- example to execute the stored procedure we just created
GO
/****** Object:  StoredProcedure [dbo].[sp_Reseed_Ingredient_Types]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Reseed_Ingredient_Types]
AS
BEGIN
    DELETE FROM Ingredient_Types
    DBCC CHECKIDENT ('Ingredient_Types', RESEED, 0)  
    -- body of the stored procedure
    INSERT INTO Ingredient_Types
    VALUES
        (NULL),
        (N'Staple'),
        (N'Spice'),
        (N'Meat'),
        (N'Protein'),
        (N'Fruit'),
        (N'Vegetable'),
        (N'Herb'),
        (N'Juice'),
        (N'Dairy')
END
-- example to execute the stored procedure we just created
GO
/****** Object:  StoredProcedure [dbo].[sp_Reseed_Ingredients]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Reseed_Ingredients]
AS
BEGIN
    DELETE FROM Ingredients
    DBCC CHECKIDENT ('Ingredients', RESEED, 0)  
    -- body of the stored procedure
    INSERT INTO Ingredients (ingredient_type_id, ingredient_name, staple)
    VALUES
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Staple'),
            N'Water',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Staple'),
            N'Salt',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Staple'),
            N'Flour',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Staple'),
            N'Sugar',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Staple'),
            N'Olive oil',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Staple'),
            N'Canola oil',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Garlic',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Black pepper',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Garlic salt',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Juice'),
            N'Lemon juice',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Juice'),
            N'Lime juice',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Egg',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Milk',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Heavy cream',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Cream cheese',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Sour cream',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Ground beef',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Steak',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Chuck roast',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Sausage',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Ham',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Bacon',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Pork roast',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Ground sausage',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Italian sausage',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Chicken',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Chicken breast',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Chicken thigh',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Chicken wing',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Protein'),
            N'Salmon',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Mayonnaise',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Barbeque sauce',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Ketchup',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Mustard',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Dijon mustard',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Dairy'),
            N'Hot sauce',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Vegetable'),
            N'Spinach',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Vegetable'),
            N'Lettuce',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Vegetable'),
            N'Tomato',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Vegetable'),
            N'Onion',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Vegetable'),
            N'Bell pepper',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Vegetable'),
            N'Jalapeno',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Turmeric',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Cumin',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Cinnamon',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Chili powder',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Cayenne pepper',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Smoked paprika',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Spice'),
            N'Paprika',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Fruit'),
            N'Apple',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Fruit'),
            N'Banana',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Fruit'),
            N'Lemon',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Fruit'),
            N'Lime',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Herb'),
            N'Oregano',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Herb'),
            N'Cilantro',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Herb'),
            N'Basil',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Herb'),
            N'Thyme',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Herb'),
            N'Rosemary',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Broth'),
            N'Beef broth',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Broth'),
            N'Chicken broth',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Broth'),
            N'Beef stock',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Broth'),
            N'Chicken stock',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Drink'),
            N'Vodka',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Drink'),
            N'Coca-Cola',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Drink'),
            N'Dr. Pepper',
            1
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Drink'),
            N'Whiskey',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Drink'),
            N'Rum',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Drink'),
            N'Beer',
            0
        ),
        (
            (SELECT ingredient_type_id FROM Ingredient_Types WHERE ingredient_type=N'Drink'),
            N'Tequila',
            0
        )

    
END
-- example to execute the stored procedure we just created
GO
/****** Object:  StoredProcedure [dbo].[sp_Reseed_Recipes]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[sp_Reseed_Recipes]

-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    DELETE FROM Recipes
    DBCC CHECKIDENT ('Recipes', RESEED, 0)

    INSERT INTO Recipes (recipe_name, recipe_description, recipe_instructions, recipe_prep_time, recipe_cook_time, recipe_cuisine_id)
    VALUES 
    (
        N'Garlic bread',
        N'Delicious garlicky buttery toast',
        N'Spread butter on bread. Sprinkle garlic salt. Broil until slightly brown and crispy',
        '00:00:03',
        '00:00:05',
        (SELECT cuisine_id FROM Cuisines WHERE cuisine_name=N'Italian')
    ),
    (
        N'Chili', --name
        N'Nice pot of spicy chili', --desc
        N'Chop spices and veggies
        Saute garlic and spices until aromatic. 
        Brown meat. 
        Add all ingredients to instant pot.
        Cook on "chili" setting
        ', --instruct
        '00:00:30', --prep
        '00:00:05', --cook
        (SELECT cuisine_id FROM Cuisines WHERE cuisine_name=N'American') --cuisine
    ),
    (
        N'Ham and Spinach Omelette',
        N'Nice omelette with ham and spinach',
        N'Put butter into pan on medium heat and let it melt
        Crack eggs in bowl and whisk with salt, pepper, and milk if desired
        Pour mixture into pan and spread it evenly around
        Cook until top is nearly solidified.
        Add ham and spinach.
        Allow spinach to wilt.
        Fold and serve with hot sauce.',
        '00:00:05',
        '00:00:03',
        (SELECT cuisine_id FROM Cuisines WHERE cuisine_name=N'American')
    ),
    (
        N'Homemade Artisan Bread',
        N'Simple homemade crusty artisan bread',
        N'Whisk flour, yeast, and salt.
        Pour in cool water and gently mix together.
        Keep working until moistened.
        Shape into ball.
        Cover and sit at room temperature for 2-3 hours.
        Dough should almost double in size.
        Recommened to let dough rest in the fridge for 12 hours - 3 days.
        Preheat oven to 475C.
        Dust nonstick baking sheet with flour or cornmeal.
        Turn dough onto floured work surface.
        Cut dough in half.
        Shape into 2 long loaves about 9"x3", about 3" apart
        Loosely cover and rest for 45 minutes.
        Score bread loaves with 3 slashes each, about 1/2" deep.
        Bake for 20-25 minutes or until the crust is golden brown.
        Let rest for 5 minutes before slicing and serving.',
        '00:04:00',
        '00:00:25',
        (SELECT cuisine_id FROM Cuisines WHERE cuisine_name=N'')
    )


GO
/****** Object:  StoredProcedure [dbo].[sp_Search_Recipe_Names]    Script Date: 2/2/2021 5:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Search_Recipe_Names] 
	-- Add the parameters for the stored procedure here
	@search_term nvarchar(50) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM Recipes
	WHERE recipe_name LIKE '%' + @search_term + '%'
END
GO
USE [master]
GO
ALTER DATABASE [C:\USERS\JKRAHN\SOURCE\REPOS\RECIPEBOOK_DB\recipe_book.mdf] SET  READ_WRITE 
GO
